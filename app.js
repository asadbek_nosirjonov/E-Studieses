 window.addEventListener('DOMContentLoaded', () => {

// MENUS ACTIVE CLASS START

    const item = document.querySelectorAll('.menus');

    function something() {
        for (const elem of document.querySelectorAll('.menus')) {
            elem.addEventListener('click',()=>{
                for (const elem of document.querySelectorAll('.menus')) {
                    elem.classList.remove('active')
                }
                elem.classList.add('active')
            })
        }
    }
    something();

// MENUS ACTIVE CLASS END
    

// SEARCH BUTTON START
    
    const searchBtn = document.querySelector('.section__search');
    const searchInput = document.querySelector('.section__search-input');

      
    function show() {
        searchInput.style.display = 'block';
    }

    searchBtn.addEventListener('click', () => {
        show()
    })

// SEARCH BUTTON END


// PROFILE START

    const profile = document.querySelector('.profile');
    const profileBtn = document.querySelector('.profileBtn');


    if(profileBtn) {
        profileBtn.addEventListener('click', () => {
            profile.classList.toggle('showProfile')
        })
    }

    const menuss = document.querySelector('.all-menus')
    console.log(menuss)

// PROFILE END


// CARDS START 

    const btnColumn = document.querySelector('.btnForColumn');
    const btnGrid = document.querySelector('.btnForGrid');
    const allCards = document.querySelectorAll('.article__cards');
    const card = document.querySelectorAll('.article__cards-card');
    const range = document.querySelectorAll('.range')

    function showColumn() {
        allCards.forEach(item => {
            item.classList.add('showColumnAllCards')
        })
    }

    function showColumnCard() {
        card.forEach(item => {
            item.classList.add('showColumnCard')
            range.forEach(item => {
                item.remove()
            })
        })
    }

        if( btnColumn ) {
            btnColumn.addEventListener('click', () => {
                showColumn()
                showColumnCard()
            })
        }


    function showGrid() {
        allCards.forEach(item => {
            item.classList.remove('showColumnAllCards')
        })
    }

    function showGridCard() {
        card.forEach(item => {
            item.classList.remove('showColumnCard')
            range.forEach(item => {
                item.remove()
            })
        })
    }
   
       if( btnGrid ) {
        btnGrid.addEventListener('click', () => {
            showGrid()
            showGridCard()
        })
       }

//CARDS END

//CHALLANGES HTML/TIME START

    function clock() {
        const hour = document.querySelector('.hour');
        const minut = document.querySelector('.minutes');
        const second = document.querySelector('.seconds');
    
        var h = new Date().getHours();
        var m = new Date().getMinutes();
        var s = new Date().getSeconds();

        if( hour && minut && second ) {
            hour.innerHTML = h
            minut.innerHTML = m
            second.innerHTML = s
        }
    }
    
    var interval = setInterval(clock, 1000);


    //CHALLANGES HTML/TIME END

});
